package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.bean.Flowers;
import com.epam.rd.java.basic.task8.builderXML.SAX_STAXBuilder;
import com.epam.rd.java.basic.task8.parsers.ParserSTAX;
import com.epam.rd.java.basic.task8.sort.SortXML;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parseXML() throws XMLStreamException, FileNotFoundException {
		return new ParserSTAX().parseXML(xmlFileName);
	}

    public void sort(Flowers flowers) {
        SortXML sortXML = new SortXML();
        sortXML.sortByWatering(flowers);
    }

    public void buildXML(Flowers flowers, String outputXmlFile)
            throws TransformerException, XMLStreamException, IOException {
        SAX_STAXBuilder builder = new SAX_STAXBuilder();
        builder.buildXML(flowers, outputXmlFile);
    }
}