package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.bean.*;
import com.epam.rd.java.basic.task8.builderXML.DOMBuilder;
import com.epam.rd.java.basic.task8.parsers.DOMParser;
import com.epam.rd.java.basic.task8.sort.SortXML;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {
	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parseXML() throws ParserConfigurationException, IOException, SAXException {
		return new DOMParser().parseXML(xmlFileName);
	}

	public void sort(Flowers flowers) {
		SortXML sortXML = new SortXML();
		sortXML.sortByTempreture(flowers);
	}

	public void buildXML(Flowers flowers, String outputXmlFile) throws ParserConfigurationException, TransformerException {
		DOMBuilder builder = new DOMBuilder();
		builder.buildXML(flowers, outputXmlFile);
	}
}


