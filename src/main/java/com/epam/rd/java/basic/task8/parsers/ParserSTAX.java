package com.epam.rd.java.basic.task8.parsers;

import com.epam.rd.java.basic.task8.bean.*;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ParserSTAX {
    private Flowers flowers = new Flowers();

    public Flowers parseXML(String xmlFileName) throws XMLStreamException, FileNotFoundException {
        FileInputStream input = new FileInputStream(new File(xmlFileName));
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = inputFactory.createXMLStreamReader(input);
        String name;

        while (reader.hasNext()) {
            int type = reader.next();
            if (type == XMLStreamConstants.START_ELEMENT) {
                name = reader.getLocalName();
                if (name.equals("flower")) {
                    Flower flower = buildFlower(reader);
                    flowers.getFlowers().add(flower);
                }
            }
        }
        return flowers;
    }

    private Flower buildFlower(XMLStreamReader reader) throws XMLStreamException {
        Flower flower = new Flower();
        String name;

        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (name) {
                        case "name":
                            flower.setName(getXMLText(reader));
                            break;
                        case "soil":
                            flower.setSoil(getXMLText(reader));
                            break;
                        case "origin":
                            flower.setOrigin(getXMLText(reader));
                            break;
                        case "visualParameters":
                            flower.setVisualParameters(getVisualParameters(reader));
                            break;
                        case "growingTips":
                            flower.setGrowingTips(getGrowingTips(reader));
                            break;
                        case "multiplying":
                            flower.setMultiplying(getXMLText(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if ("flower".equals(name)) {
                        return flower;
                    }
            }
        }
        throw new XMLStreamException("Unknown element in tag Flower");
    }

    private GrowingTips getGrowingTips(XMLStreamReader reader) throws XMLStreamException {
        GrowingTips growingTips = new GrowingTips();
        int type;
        String name;

        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (name) {
                        case "tempreture":
                            String attributeT = reader.getAttributeValue(null, "measure");
                            String valueT = getXMLText(reader);
                            growingTips.setTempreture(new Parameter(
                                    attributeT, Integer.parseInt(valueT)));
                            break;
                        case "lighting":
                            growingTips.setLighting(reader.getAttributeValue(
                                    null, "lightRequiring"));
                            break;
                        case "watering":
                            String attributeW = reader.getAttributeValue(null, "measure");
                            String valueW = getXMLText(reader);
                            growingTips.setWatering(new Parameter(
                                    attributeW, Integer.parseInt(valueW)));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("growingTips")){
                        return growingTips;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag GrowingTips");
    }

    private VisualParameters getVisualParameters(XMLStreamReader reader) throws XMLStreamException {
        VisualParameters visualParameters = new VisualParameters();
        int type;
        String name;

        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (name) {
                        case "stemColour":
                            visualParameters.setStemColour(getXMLText(reader));
                            break;
                        case "leafColour":
                            visualParameters.setLeafColour(getXMLText(reader));
                            break;
                        case "aveLenFlower":
                            String attribute = reader.getAttributeValue(null, "measure");
                            String value = getXMLText(reader);
                            visualParameters.setParameter(new Parameter(
                                    attribute, Integer.parseInt(value)));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (name.equals("visualParameters")){
                        return visualParameters;
                    }
                    break;
            }
        }
        throw new XMLStreamException("Unknown element in tag VisualParameters");
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}
