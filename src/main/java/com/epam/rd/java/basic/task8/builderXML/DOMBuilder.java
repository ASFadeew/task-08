package com.epam.rd.java.basic.task8.builderXML;

import com.epam.rd.java.basic.task8.bean.Flower;
import com.epam.rd.java.basic.task8.bean.Flowers;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class DOMBuilder {
    public void buildXML(Flowers flowers, String outputXmlFile) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.newDocument();
        Element rootElement = document.createElement("flowers");
        rootElement.setAttribute("xmlns", "http://www.nure.ua");
        rootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
        document.appendChild(rootElement);

        for (Flower flower : flowers.getFlowers()) {
            Element flowerTag = document.createElement("flower");
            rootElement.appendChild(flowerTag);
            createElement(document, flowerTag, "name", flower.getName());
            createElement(document, flowerTag, "soil", flower.getSoil().getSoil());
            createElement(document, flowerTag, "origin", flower.getOrigin());
            createVisualParameters(document, flower, flowerTag);
            createGrowingTips(document, flower, flowerTag);
            createElement(document, flowerTag, "multiplying", flower.getMultiplying().getMultiplying());
        }
        writeXML(document, outputXmlFile);
    }

    private void createElement(Document document, Element flower, String tagName, String value) {
        Element element;
        element = document.createElement(tagName);
        element.setTextContent(value);
        flower.appendChild(element);
    }

    private void createVisualParameters(Document document, Flower f, Element flower) {
        Element visualParameters = document.createElement("visualParameters");
        flower.appendChild(visualParameters);
        createElement(document, visualParameters, "stemColour", f.getVisualParameters().getStemColour());
        createElement(document, visualParameters, "leafColour", f.getVisualParameters().getLeafColour());

        Element aveLenFlower = document.createElement("aveLenFlower");
        aveLenFlower.setAttribute("measure", f.getVisualParameters().getParameter().getMeasure());
        aveLenFlower.setTextContent(String.valueOf(f.getVisualParameters().getParameter().getValue()));
        visualParameters.appendChild(aveLenFlower);
    }

    private void createGrowingTips(Document document, Flower f, Element flower) {
        Element growingTips = document.createElement("growingTips");
        flower.appendChild(growingTips);

        Element tempreture = document.createElement("tempreture");
        tempreture.setAttribute("measure", f.getGrowingTips().getTempreture().getMeasure());
        tempreture.setTextContent(String.valueOf(f.getGrowingTips().getTempreture().getValue()));
        growingTips.appendChild(tempreture);

        Element lighting = document.createElement("lighting");
        lighting.setAttribute("lightRequiring", f.getGrowingTips().getLighting());
        growingTips.appendChild(lighting);

        Element watering = document.createElement("watering");
        watering.setAttribute("measure", f.getGrowingTips().getWatering().getMeasure());
        watering.setTextContent(String.valueOf(f.getGrowingTips().getWatering().getValue()));
        growingTips.appendChild(watering);
    }

    private void writeXML(Document document, String outputXmlFile) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new File(outputXmlFile));

        transformer.transform(source, result);
    }
}
