package com.epam.rd.java.basic.task8.bean;

import java.util.ArrayList;
import java.util.List;

public class Flowers {
    private List<Flower> flowers = new ArrayList<>();

    public List<Flower> getFlowers() {
        return flowers;
    }
}
