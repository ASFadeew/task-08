package com.epam.rd.java.basic.task8.builderXML;

import com.epam.rd.java.basic.task8.bean.Flower;
import com.epam.rd.java.basic.task8.bean.Flowers;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SAX_STAXBuilder {
    
    public void buildXML(Flowers flowers, String outputXmlFile) throws XMLStreamException, IOException, TransformerException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        writeXml(out, flowers);

        String xml = new String(out.toByteArray(), StandardCharsets.UTF_8);
        String prettyPrintXML = formatXML(xml);

        Files.writeString(Paths.get(outputXmlFile), prettyPrintXML, StandardCharsets.UTF_8);
    }

    private String formatXML(String xml) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

        StreamSource source = new StreamSource(new StringReader(xml));
        StringWriter output = new StringWriter();
        transformer.transform(source, new StreamResult(output));
        return output.toString();
    }

    private void writeXml(ByteArrayOutputStream out, Flowers flowers) throws XMLStreamException {
        XMLOutputFactory output = XMLOutputFactory.newInstance();
        XMLStreamWriter writer = output.createXMLStreamWriter(out);
        writer.writeStartDocument("utf-8", "1.0");

        writer.writeStartElement("flowers");
        writer.writeAttribute("xmlns", "http://www.nure.ua");
        writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        writer.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

        for (Flower flower : flowers.getFlowers()) {
            writer.writeStartElement("flower");

            writer.writeStartElement("name");
            writer.writeCharacters(flower.getName());
            writer.writeEndElement();

            writer.writeStartElement("soil");
            writer.writeCharacters(flower.getSoil().getSoil());
            writer.writeEndElement();

            writer.writeStartElement("origin");
            writer.writeCharacters(flower.getOrigin());
            writer.writeEndElement();

            writer.writeStartElement("visualParameters");
            writer.writeStartElement("stemColour");
            writer.writeCharacters(flower.getVisualParameters().getStemColour());
            writer.writeEndElement();
            writer.writeStartElement("leafColour");
            writer.writeCharacters(flower.getVisualParameters().getLeafColour());
            writer.writeEndElement();
            writer.writeStartElement("aveLenFlower");
            writer.writeAttribute("measure", flower.getVisualParameters().getParameter().getMeasure());
            writer.writeCharacters(String.valueOf(flower.getVisualParameters().getParameter().getValue()));
            writer.writeEndElement();
            writer.writeEndElement();

            writer.writeStartElement("growingTips");
            writer.writeStartElement("tempreture");
            writer.writeAttribute("measure", flower.getGrowingTips().getTempreture().getMeasure());
            writer.writeCharacters(String.valueOf(flower.getGrowingTips().getTempreture().getValue()));
            writer.writeEndElement();
            writer.writeStartElement("lighting");
            writer.writeAttribute("lightRequiring", flower.getGrowingTips().getLighting());
            writer.writeEndElement();
            writer.writeStartElement("watering");
            writer.writeAttribute("measure", flower.getGrowingTips().getWatering().getMeasure());
            writer.writeCharacters(String.valueOf(flower.getGrowingTips().getWatering().getValue()));
            writer.writeEndElement();
            writer.writeEndElement();

            writer.writeStartElement("multiplying");
            writer.writeCharacters(flower.getMultiplying().getMultiplying());
            writer.writeEndElement();

            writer.writeEndElement();
        }
        writer.writeEndDocument();

        writer.flush();
        writer.close();
    }
}
