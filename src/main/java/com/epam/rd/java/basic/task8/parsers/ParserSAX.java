package com.epam.rd.java.basic.task8.parsers;

import com.epam.rd.java.basic.task8.bean.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

public class ParserSAX {
    private Flowers flowers = new Flowers();

    public Flowers parseXML(String xmlFileName) throws IOException, SAXException, ParserConfigurationException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();
        SAXHandler handler = new SAXHandler();
        parser.parse(xmlFileName, handler);
        return flowers;
    }

    private class SAXHandler extends DefaultHandler {
        Flower flower;
        GrowingTips growingTips;
        Parameter parameter;
        VisualParameters visualParameters;
        StringBuilder sb = new StringBuilder();
        String value;

        @Override
        public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException {
            sb.setLength(0);
            value = qName;
            switch (value) {
                case "flower":
                    flower = new Flower();
                    break;
                case "visualParameters":
                    visualParameters = new VisualParameters();
                    break;
                case "growingTips":
                    growingTips = new GrowingTips();
                    break;
                case "aveLenFlower":
                case "tempreture":
                case "watering":
                    parameter = new Parameter();
                    parameter.setMeasure(attributes.getValue("measure"));
                    break;
                case "lighting":
                    growingTips.setLighting(attributes.getValue("lightRequiring"));
                    break;
                default:break;
            }
        }

        @Override
        public void endElement (String uri, String localName, String qName) throws SAXException {
            switch (qName) {
                case "aveLenFlower":
                    visualParameters.setParameter(parameter);
                    break;
                case "tempreture":
                    growingTips.setTempreture(parameter);
                    break;
                case "watering":
                    growingTips.setWatering(parameter);
                    break;
                case "visualParameters":
                    flower.setVisualParameters(visualParameters);
                    break;
                case "growingTips":
                    flower.setGrowingTips(growingTips);
                    break;
                case "flower":
                    flowers.getFlowers().add(flower);
                default:break;
            }
        }

        @Override
        public void characters (char ch[], int start, int length) throws SAXException {
            sb.append(ch, start, length);
            switch (value) {
                case "name":
                    flower.setName(sb.toString().trim());
                    break;
                case "soil":
                    flower.setSoil(sb.toString().trim());
                    break;
                case "origin":
                    flower.setOrigin(sb.toString().trim());
                    break;
                case "stemColour":
                    visualParameters.setStemColour(sb.toString().trim());
                    break;
                case "leafColour":
                    visualParameters.setLeafColour(sb.toString().trim());
                    break;
                case "aveLenFlower":
                case "tempreture":
                case "watering":
                    parameter.setValue(Integer.parseInt(sb.toString().trim()));
                    break;
                case "multiplying":
                    flower.setMultiplying(sb.toString().trim());
                    break;
                default:break;
            }
        }
    }
}
