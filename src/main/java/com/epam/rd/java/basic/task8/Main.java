package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.bean.Flowers;
import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.validation.XSDValidator;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);

		XSDValidator validator = new XSDValidator();
		validator.validate(xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		Flowers flowers = domController.parseXML();

		// sort (case 1)
		domController.sort(flowers);
		
		// save
		String outputXmlFile = "output.dom.xml";
		domController.buildXML(flowers, outputXmlFile);
        validator.validate(outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		flowers = saxController.parseXML();
		
		// sort  (case 2)
		saxController.sort(flowers);
		
		// save
		outputXmlFile = "output.sax.xml";
		saxController.buildXML(flowers, outputXmlFile);
		validator.validate(outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		flowers = staxController.parseXML();
		
		// sort  (case 3)
		staxController.sort(flowers);
		
		// save
		outputXmlFile = "output.stax.xml";
        staxController.buildXML(flowers, outputXmlFile);
        validator.validate(outputXmlFile);
	}

}
