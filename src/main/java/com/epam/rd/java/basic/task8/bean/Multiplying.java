package com.epam.rd.java.basic.task8.bean;

public enum Multiplying {
    LEAVES ("листья"),
    STEMS ("черенки"),
    SEEDS ("семена");

    private String multiplying;

    Multiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public String getMultiplying() {
        return multiplying;
    }
}
