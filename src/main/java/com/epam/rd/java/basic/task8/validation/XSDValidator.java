package com.epam.rd.java.basic.task8.validation;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.*;
import java.io.File;
import java.io.IOException;

public class XSDValidator {

    public void validate(String fileName) {
        String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
        String schemaName = "input.xsd";
        SchemaFactory factory = SchemaFactory.newInstance(language);
        File schemaLocation = new File(schemaName);

        try {
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource(fileName);
            validator.validate(source);
            System.out.println(fileName + " is valid.");
        } catch (SAXException | IOException e) {
            System.err.print("validation "+ fileName + " is not valid because " + e.getMessage());
        }
    }
}
