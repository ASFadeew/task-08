package com.epam.rd.java.basic.task8.bean;

public class Parameter {
    private String measure;
    private int value;

    public Parameter() { }

    public Parameter(String measure, int value) {
        this.measure = measure;
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
