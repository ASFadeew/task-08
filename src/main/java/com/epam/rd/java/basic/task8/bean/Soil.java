package com.epam.rd.java.basic.task8.bean;

public enum Soil {
    PODZOLIC ("подзолистая"),
    GROUND ("грунтовая"),
    SOD_PODZOLIC ("дерново-подзолистая");

    private String soil;

    Soil(String soil) {
        this.soil = soil;
    }

    public String getSoil() {
        return soil;
    }
}
