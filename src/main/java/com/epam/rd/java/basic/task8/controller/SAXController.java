package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.bean.Flowers;
import com.epam.rd.java.basic.task8.builderXML.SAX_STAXBuilder;
import com.epam.rd.java.basic.task8.parsers.ParserSAX;
import com.epam.rd.java.basic.task8.sort.SortXML;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parseXML() throws ParserConfigurationException, SAXException, IOException {
		return new ParserSAX().parseXML(xmlFileName);
	}

	public void sort(Flowers flowers) {
		SortXML sortXML = new SortXML();
		sortXML.sortByName(flowers);
	}

    public void buildXML(Flowers flowers, String outputXmlFile)
			throws TransformerException, IOException, XMLStreamException {
        SAX_STAXBuilder builder = new SAX_STAXBuilder();
        builder.buildXML(flowers, outputXmlFile);
    }

}