package com.epam.rd.java.basic.task8.bean;

public class Flower {
    private String name;
    private Soil soil;
    private String origin;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private Multiplying multiplying;

    public Flower() { }

    public Flower(String name, String soil, String origin, VisualParameters visualParameters,
                  GrowingTips growingTips, String multiplying) {
        this.name = name;
        this.soil = chooseSoil(soil);
        this.origin = origin;
        this.visualParameters = visualParameters;
        this.growingTips = growingTips;
        this.multiplying = chooseMultiplying(multiplying);
    }

    private Multiplying chooseMultiplying(String multiplying) {
        for (Multiplying m : Multiplying.values()) {
            if (m.getMultiplying().equals(multiplying)) {
                return m;
            }
        }
        return null;
    }

    private Soil chooseSoil(String soil) {
        for (Soil s : Soil.values()) {
            if (s.getSoil().equals(soil)) {
                return s;
            }
        }
        return null;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Soil getSoil() {
        return this.soil;
    }

    public void setSoil(String soil) {
        this.soil = chooseSoil(soil);
    }

    public String getOrigin() {
        return this.origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return this.visualParameters;
    }

    public void setVisualParameters(VisualParameters parameters) {
        this.visualParameters = parameters;
    }

    public GrowingTips getGrowingTips() {
        return this.growingTips;
    }

    public void setGrowingTips(GrowingTips tips) {
        this.growingTips = tips;
    }

    public Multiplying getMultiplying() {
        return this.multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = chooseMultiplying(multiplying);
    }
}







