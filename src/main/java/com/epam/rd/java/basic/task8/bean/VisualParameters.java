package com.epam.rd.java.basic.task8.bean;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private Parameter parameter;

    public VisualParameters() { }

    public VisualParameters(String stemColour, String leafColour, Parameter parameter) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.parameter = parameter;
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public Parameter getParameter() {
        return parameter;
    }

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }
}
