package com.epam.rd.java.basic.task8.parsers;

import com.epam.rd.java.basic.task8.bean.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class DOMParser {

    public Flowers parseXML(String xmlFileName) throws ParserConfigurationException, IOException, SAXException {
        Flowers flowers = new Flowers();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new File(xmlFileName));

        NodeList nodeList = document.getElementsByTagName("flower");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.hasChildNodes()) {
                flowers.getFlowers().add(parseNode(node.getChildNodes()));
            }
        }
        return flowers;
    }

    private Flower parseNode(NodeList childNode) {
        String name = childNode.item(1).getTextContent();
        String soil = childNode.item(3).getTextContent();
        String country = childNode.item(5).getTextContent();
        VisualParameters parameters = getParameters(childNode.item(7).getChildNodes());
        GrowingTips tips = getGrowingTips(childNode.item(9).getChildNodes());
        String multiplying = childNode.item(11).getTextContent();
        return new Flower(name, soil, country, parameters, tips, multiplying);
    }

    private GrowingTips getGrowingTips(NodeList childNodes) {
        Parameter tempreture = new Parameter(childNodes.item(1).getAttributes()
                .getNamedItem("measure").getNodeValue(),
                Integer.parseInt(childNodes.item(1).getTextContent()));
        String lighting = childNodes.item(3).getAttributes().getNamedItem("lightRequiring").getNodeValue();
        Parameter watering = new Parameter(childNodes.item(5).getAttributes()
                .getNamedItem("measure").getNodeValue(),
                Integer.parseInt(childNodes.item(5).getTextContent()));

        return new GrowingTips(tempreture, lighting, watering);
    }

    private VisualParameters getParameters(NodeList childNodes) {
        String stemColour = childNodes.item(1).getTextContent();
        String leafColour = childNodes.item(3).getTextContent();
        Parameter parameter = new Parameter(childNodes.item(5).getAttributes()
                .getNamedItem("measure").getNodeValue(),
                Integer.parseInt(childNodes.item(5).getTextContent()));
        return new VisualParameters(stemColour, leafColour, parameter);
    }
}
