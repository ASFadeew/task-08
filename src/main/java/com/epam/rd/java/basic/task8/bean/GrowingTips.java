package com.epam.rd.java.basic.task8.bean;

public class GrowingTips {
    private Parameter tempreture;
    private String lighting;
    private Parameter watering;

    public GrowingTips() { }

    public GrowingTips(Parameter tempreture, String lighting, Parameter watering) {
        this.tempreture = tempreture;
        this.lighting = lighting;
        this.watering = watering;
    }

    public Parameter getTempreture() {
        return tempreture;
    }

    public void setTempreture(Parameter tempreture) {
        this.tempreture = tempreture;
    }

    public String getLighting() {
        return lighting;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public Parameter getWatering() {
        return watering;
    }

    public void setWatering(Parameter watering) {
        this.watering = watering;
    }
}

