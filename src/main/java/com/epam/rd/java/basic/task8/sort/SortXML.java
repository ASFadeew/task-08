package com.epam.rd.java.basic.task8.sort;

import com.epam.rd.java.basic.task8.bean.Flowers;

import java.util.Comparator;

public class SortXML {
    public void sortByTempreture(Flowers flowers) {
        flowers.getFlowers().sort(Comparator.comparing(flower -> -flower.getGrowingTips()
                .getTempreture().getValue()));
    }

    public void sortByName(Flowers flowers) {
        flowers.getFlowers().sort(Comparator.comparing(flower -> flower.getName()));
    }

    public void sortByWatering(Flowers flowers) {
        flowers.getFlowers().sort(Comparator.comparing(flower -> -flower.getGrowingTips()
                .getWatering().getValue()));
    }
}
